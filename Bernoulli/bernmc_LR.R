# generate mcdist for bernoulli
# generates the cumulative sums of bernouli 
# and stores likelihood ratio statistic
rm(list=ls(all=TRUE))
date()
set.seed(151)
T <- 200
B <- 75000
maxc <- 50000
mcdist <- array(0, c(T - 1, maxc, T + 1))
c <- double(T + 1) # c[m] = number of samples with S_T = m
j <- double(T + 1)
for(sT in 1:(T - 1)){
  p <- sT/T # p such that E(Bin(T, p)) = sT
  for(v in 1:B){
    X1 <- rbinom(T, 1, p)
    S1 <- cumsum(X1)
    m <- S1[T]
    m1 <- m + 1 # for array indexing in R
    j[m1] <- j[m1] + 1
    if(j[m1] <= maxc){
      c[m1] <- c[m1] + 1
      #      n <- T - S1[T]
      mcdist[ , j[m1], m1] <- S1[1:(T-1)]
    }
  }
}
#mcdist

LRmcdist <- array(0, c(maxc, T + 1))
for(st in 0:T)
{
  h0 = ((st/T)^st)*( (1- st/T)^(T-st) )
  y1 = array(0, c(T - 1, maxc)) # stores simulated likelihood H1
  S_sim = mcdist[, , (st+1)]

  for(t in 1:(T-1))
    { 
      s = S_sim[t, ]
      Pi1 = s/t
      Pi2 = (st-s)/(T-t)
      y1[t,]=(Pi1^s)*((1-Pi1)^(t-s))*(Pi2^(st-s))*((1-Pi2)^(T-t-st+s))   
    }

for(l in 1:maxc) {LRmcdist[l,(st+1)] = log( max(y1[ ,l])/h0 ) }

}

filename <- paste("T",T,"_logLR_dist50000.RData", sep = "")
save(LRmcdist, file = filename)
date()