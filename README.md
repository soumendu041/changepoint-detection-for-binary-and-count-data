# Changepoint detection for binary and count data

In this repository, we provide companion R code for the paper [1].

The following table lists relevant code for generating the figures corresponding to the simulations in [1].

| Figure # | Relevant code                        |
| ---      | ---                                  |
| 1        | `Bernoulli/ber_multicomp_para_new.R` |
| 2        | `Poisson/pois_cpd_multicom_para.R`   |
| 3        | `Poisson/po.cond-perm-para.R`        |
| 4        | `Bernoulli/glob_loc_MT_para.R`       |
| 5        | `Possion/po.glob_loc_MT_para.R`      |

Some of the above scripts use pre-generated Monte Carlo distributions stored in `.RData` files. These can be generated from scratch using the scripts listed in the following table.

| `.RData` files in | Relevant code                                                                  |
| ---               | ---                                                                            |
| Bernoulli         | `bernmc_cu5.R`, `bernmc_cu1.R`, `bernmc_LR.R`,`bernmc_minP.R`                  |
| Poisson           | `poismc_cu0.R`, `poismc_cu1.R`, `poismc_cu5.R`, `poismc_LR.R`, `poismc_minP.R` |
| BrownianBridge    | `BB_new.R`                                                                     |

### References
1. Shyamal K. De and Soumendu Sundar Mukherjee. [Exact Tests for Offline Changepoint Detection in Multichannel Binary and Count Data with Application to Networks](https://arxiv.org/abs/2008.09083). arXiv:2008.09083, 2020.

