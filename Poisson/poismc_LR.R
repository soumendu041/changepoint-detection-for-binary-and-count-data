# generate mcdist for Poisson
# generates the cumulative sums of Poisson
# and finally stores LR statistic
rm(list=ls(all=TRUE))
date()
tictoc::tic()
set.seed(151)
T <- 50
B <- 80000
maxc <- 50000
STmax<- 250 # max possible value that ST=sum(X_i) can take
uplim <- floor(6*sqrt(STmax))+1
mcdist <- array(0, c(T - 1, maxc, STmax+uplim))
c <- double(STmax+uplim) # c[m] = number of samples with S_T = m
j <- double(STmax+uplim)
for(sT in 1:STmax){
  ptm0 <- proc.time()  # recording time
  lambda <- sT/T # lambda such that E(Pois(T*lambda)) = sT
  for(v in 1:B){
    X1 <- rpois(T, lambda)
    S1 <- cumsum(X1)
    m <- S1[T]
    m1 <- m + 1 # for array indexing in R
    j[m1] <- j[m1] + 1
    if(j[m1] <= maxc){
      c[m1] <- c[m1] + 1
      mcdist[ , j[m1], m1] <- S1[1:(T-1)]
    }
  }
  ptm1=proc.time() - ptm0  # recording time
  jnk=as.numeric(ptm1[3])
  if(sT %% 10==0){ cat(sT,jnk,"\n") }
}

po.LRmcdist <- array(0, c(maxc, STmax + 1))
for (st in 0:STmax)
{
  ptm0 <- proc.time()  # recording time
  if (st > 0) {
    h0 = log( ((st/T)*exp(-1))^(st) )
  } else
    h0 = 0 #log-likelihood under H0 upto constant
  y1 = array(0, c(T - 1, maxc)) # stores simulated likelihood H1
  S_sim = mcdist[, , (st + 1)]
  
  for (t in 1:(T - 1))
  {
    s = S_sim[t,]
    lam1 = s / t
    lam2 = (st - s) / (T - t)
    for (l in 1:maxc)
      # log-likelihood under H1 upto constant
    {
      if (s[l] == 0) {
        y1[t, l] = log((exp(-1) * lam2[l]) ^ (st - s[l]))
      }
      if (s[l] == st) {
        y1[t, l] = log((exp(-1) * lam1[l]) ^ s[l])
      }
      if (s[l] > 0 & s[l] < st)
      {
        y1[t, l] = log( (exp(-1)*lam1[l])^(s[l]) ) + log( (exp(-1)*lam2[l])^(st - s[l]) )
      }
      
    }
    
  }
  
  for (l in 1:maxc) {
    po.LRmcdist[l, (st + 1)] =  max(y1[, l]) - h0
  }
  
  ptm1 = proc.time() - ptm0  # recording time
  jnk = as.numeric(ptm1[3])
  if (st %% 10 == 0) {
    cat(st, jnk, "\n")
  }
}

filename <- paste("T",T,"_po.logLR_maxc50000ST250.RData", sep = "")
save(po.LRmcdist, file = filename)
date()
tictoc::tic()
