# parallel computing for....
# change point testing for poisson in 1 channel
# using conditional tests and permutations tests using
# (1) min pvalue binomial exact test, (2) likelihood ratio 
# (3,4) cusum tests for delta=1/2,1, 
#======================================================================
rm(list=ls(all=TRUE))
date()
tictoc::tic()
library(changepoint)

#============ simulation setup =============
set.seed(151)
T = 50
alpha = 0.1
mcrun = 500 
tau =  10 #(T/2) # 1st change point##tau2=20 # 2nd change point
lam1 = 0.15  # pre-change poisson mean
lam2 = 1.5 # post-change poisson mean
#lam = 1 # poisson mean for generating data under H0
theta = 1 # 0 or 1 indicating H0 true or false
#==========================================

filename.S <- paste("T",T,"_po.minP_maxc50000ST250.RData", sep = "")
load(filename.S)
filename.S <- paste("T",T,"_po.logLR_maxc50000ST250.RData", sep = "")
load(filename.S)
filename.S <- paste("T",T,"_po.cu5_maxc50000ST250.RData", sep = "")
load(filename.S)
filename.S <- paste("T",T,"_po.cu1_maxc50000ST250.RData", sep = "")
load(filename.S)
# filename.S <- paste("T",T,"_po.cu0_maxc10000ST1000.RData", sep = "")
# load(filename.S)
# load("../BrownianBridge/BB_0.RData")
# load("../BrownianBridge/BB_0.5.RData")
# load("../BrownianBridge/BB_1.RData")
#=======================================================


## (0)----------------------------------
cusum = function(x,l,u,del) # with factor (t/T)*(1- (t/T))
{
  T = length(x)
  X = rep(0, T)
  Y = X
  Z = X
  for(t in max(1, floor(l*T)): min(floor(u*T), T-1))
  { c = ( (t/T)*(1- (t/T)) )^(del)
  X[t] = c*mean(x[1:t])
  Y[t] = c*mean(x[(t+1):T])
  }
  Z = abs(X - Y)
  return(max(Z))
}



#(1a) exact conditional minP test: returns pval for each channel 
po.minPpval = function(X) 
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  m <- S[T]
  pval <- double(T - 1)
  ## Compute p-values
  for(t in 1:(T-1)){
    ob <- dbinom(S[t], m, t/T)
    lst = dbinom(0:m, m, t/T)
    pval[t] = sum(lst[which(lst<=ob)])
  }
  p1obs = min(pval)
  pvalue = mean(po.minPmcdist[ ,(ST+1)]<= p1obs)
  return(pvalue)
}


#(1b) minP permutation test: returns pval for each channel 
perm.minPpval = function(X, B=1000) 
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  m <- S[T]
  pval <- double(T - 1)
  ## Compute p-values
  for(t in 1:(T-1)){
    ob <- dbinom(S[t], m, t/T)
    lst = dbinom(0:m, m, t/T)
    pval[t] = sum(lst[which(lst<=ob)])
  }
  p1obs = min(pval)
  
  Sb = rep(0,B)
#  set.seed(151)
  for(j in 1:B)
  {
    perm = sample(1:T)
    Xperm = X[perm]
    
    S <- cumsum(Xperm)
    pval <- double(T - 1)
    ## Compute p-values
    for(t in 1:(T-1)){
      ob <- dbinom(S[t], m, t/T)
      lst = dbinom(0:m, m, t/T)
      pval[t] = sum(lst[which(lst<=ob)])
    }
    Sb[j] = min(pval)
 
  }
  if(var(X)==0){pvalue = 1} else
  { pvalue = mean(Sb<= p1obs) }
  
  return(pvalue)
}



#(2a) exact conditional likelihood ratio test: returns pval for each channel 
po.LRpval = function(X)  # returns pvalue using cusum given ST
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  
  ## Compute observed LR
  la1 = rep(0, T-1)
  la2 = la1
  h1 = la1
  for(t in 1:(T-1))
  { 
    la1[t] = S[t]/t
    la2[t] = (ST-S[t])/(T-t)  #mean(X[(t+1):T]) 
    # log-likelihood under H1 upto constant
    if(S[t]==0){h1[t] = log( (exp(-1)*la2[t])^(ST-S[t]) )}
    if(S[t]==ST){h1[t] = log( (exp(-1)*la1[t])^(S[t]) ) }
    if(S[t]>0 & S[t]<ST)
    {h1[t]=log((exp(-1)*la1[t])^(S[t]))+log((exp(-1)*la2[t])^(ST-S[t]))}
  }
  if(ST>0){h0=log( ((ST/T)*exp(-1))^(ST) )} else h0=0 # log-likelihood under H0 upto constant
  po.logLR.obs = max(h1)-h0
  pvalue = mean(po.LRmcdist[,(ST+1)] >= po.logLR.obs)
  return(pvalue)
}



#(2b) likelihood ratio permutation test: returns pval for each channel 
perm.LRpval = function(X, B=1000)  # returns pvalue using cusum given ST
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  
  ## Compute observed LR
  la1 = rep(0, T-1); la2 = la1; h1 = la1
  for(t in 1:(T-1))
  { 
    la1[t] = S[t]/t
    la2[t] = (ST-S[t])/(T-t)  #mean(X[(t+1):T]) 
    # log-likelihood under H1 upto constant
    if(S[t]==0){h1[t] = log( (exp(-1)*la2[t])^(ST-S[t]) )}
    if(S[t]==ST){h1[t] = log( (exp(-1)*la1[t])^(S[t]) ) }
    if(S[t]>0 & S[t]<ST)
    {h1[t]= log(((exp(-1)*la1[t]) )^(S[t])) + log((exp(-1)*la2[t])^(ST-S[t]))}
  }
  if(ST>0){h0=log( ((ST/T)*exp(-1))^(ST) ) } else h0=0 # log-likelihood under H0 upto constant
  po.logLR.obs = max(h1)-h0
  

  Sb = rep(0,B)
#  set.seed(151)
  for(j in 1:B)
  {
    perm = sample(1:T)
    Xperm = X[perm]
    
    S = cumsum(Xperm)
    
    ## Compute observed LR
    la1 = rep(0, T-1); la2 = la1; h1 = la1
    for(t in 1:(T-1))
    { 
      la1[t] = S[t]/t
      la2[t] = (ST-S[t])/(T-t)   
      # log-likelihood under H1 upto constant
      if(S[t]==0){h1[t] = log( (exp(-1)*la2[t])^(ST-S[t]) )}
      if(S[t]==ST){h1[t] = log( (exp(-1)*la1[t])^(S[t]) ) }
      if(S[t]>0 & S[t]<ST)
      {h1[t]= log(((exp(-1)*la1[t]) )^(S[t])) + log((exp(-1)*la2[t])^(ST-S[t]))}
    }
    Sb[j] = max(h1)-h0
  }
  
  if(var(X)==0){pvalue = 1} else
  { pvalue = mean(Sb>=po.logLR.obs) }  
    return(pvalue)
}




#(3a) extact conditional CU.5 test: returns pval for each channel 
po.cu5pval = function(X,l=0,u=1,del=0.5)  # returns pvalue using cusum given ST
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  a <- max(1,floor(l*T)) # sum starts from a
  b <- min(floor(u*T), T-1) # last sum taken at b
  ## Compute observed cusum
  Y1 = double(b-a+1)
  Y2 = Y1
  Y3 = Y1
  for(t in a:b)
  { 
    Y1[t] = mean(X[1:t])
    Y2[t] = mean(X[(t+1):T]) 
    Y3[t] = ( ((t/T)*(1- t/T))^del )*((Y1[t] - Y2[t]))
  }
  cu.obs = max(abs(Y3))
  pvalue = mean(po.cu5mcdist[,(ST+1)] >= cu.obs)
  return(pvalue)
}



#(3b) CU.5 permutation test: returns pval for each channel 
perm.cu5pval = function(X,l=0,u=1,del=0.5, B=1000)  # returns pvalue using cusum given ST
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  a <- max(1,floor(l*T)) # sum starts from a
  b <- min(floor(u*T), T-1) # last sum taken at b
  ## Compute observed cusum
  Y1 = double(b-a+1); Y2 = Y1; Y3 = Y1
  for(t in a:b)
  { 
    Y1[t] = mean(X[1:t])
    Y2[t] = mean(X[(t+1):T]) 
    Y3[t] = ( ((t/T)*(1- t/T))^del )*((Y1[t] - Y2[t]))
  }
  cu.obs = max(abs(Y3))
  
  Sb = rep(0,B)
#  set.seed(151)
  for(j in 1:B)
  {
    perm = sample(1:T)
    Xperm = X[perm]
    
    ## Compute observed cusum
    Y1 = double(b-a+1); Y2 = Y1; Y3 = Y1
    for(t in a:b)
    { 
      Y1[t] = mean(Xperm[1:t])
      Y2[t] = mean(Xperm[(t+1):T]) 
      Y3[t] = ( ((t/T)*(1- t/T))^del )*((Y1[t] - Y2[t]))
    }
    Sb[j] = max(abs(Y3))
  }
  
  if(var(X)==0){pvalue = 1} else
  { pvalue = mean(Sb>=cu.obs) }  
  
  return(pvalue)
}



#(4a) extact conditional CU1 test: returns pval for each channel 
po.cu1pval = function(X,l=0,u=1,del=1)  # returns pvalue using cusum given ST
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  a <- max(1,floor(l*T)) # sum starts from a
  b <- min(floor(u*T), T-1) # last sum taken at b
  ## Compute observed cusum
  Y1 = double(b-a+1)
  Y2 = Y1
  Y3 = Y1
  for(t in a:b)
  { 
    Y1[t] = mean(X[1:t])
    Y2[t] = mean(X[(t+1):T]) 
    Y3[t] = ( ((t/T)*(1- t/T))^del )*((Y1[t] - Y2[t]))
  }
  cu.obs = max(abs(Y3))
  pvalue = mean(po.cu1mcdist[,(ST+1)] >= cu.obs)
  return(pvalue)
}



#(4b) CU1 permutation test: returns pval for each channel 
perm.cu1pval = function(X,l=0,u=1,del=1, B=1000)  # returns pvalue using cusum given ST
{
  T = length(X)
  ST <- sum(X)
  S <- cumsum(X)
  a <- max(1,floor(l*T)) # sum starts from a
  b <- min(floor(u*T), T-1) # last sum taken at b
  ## Compute observed cusum
  Y1 = double(b-a+1); Y2 = Y1; Y3 = Y1
  for(t in a:b)
  { 
    Y1[t] = mean(X[1:t])
    Y2[t] = mean(X[(t+1):T]) 
    Y3[t] = ( ((t/T)*(1- t/T))^del )*((Y1[t] - Y2[t]))
  }
  cu.obs = max(abs(Y3))
  
  Sb = rep(0,B)
#  set.seed(151)
  for(j in 1:B)
  {
    perm = sample(1:T)
    Xperm = X[perm]
    
    #S = cumsum(Xperm)
    ## Compute observed cusum
    Y1 = double(b-a+1); Y2 = Y1; Y3 = Y1
    for(t in a:b)
    { 
      Y1[t] = mean(Xperm[1:t])
      Y2[t] = mean(Xperm[(t+1):T]) 
      Y3[t] = ( ((t/T)*(1- t/T))^del )*((Y1[t] - Y2[t]))
    }
    Sb[j] = max(abs(Y3))
  }
  
  if(var(X)==0){pvalue = 1} else
  { pvalue = mean(Sb>=cu.obs) }  
  
  return(pvalue)
}





#==== preparing for parrallel computing using all cores =========
library(pbmcapply)     # Track Progress of mclpply with progress bar
library(parallel)            # base package for parallel computing
library(doParallel)          # for using registerDoParallel
no_cores = detectCores()     # find number of cores in system
registerDoParallel(no_cores) # creating implicit clusters
set.seed( 151, kind = "L'Ecuyer-CMRG" )
#==========================================================

#===== MCrun begins ========

output = pbmclapply(1:mcrun, function(i){
  #---- data generation ----
  if(theta==0){X=rpois(T,lam)}
  if(theta==1){X=c(rpois(tau,lam1),rpois(T-tau,lam2))}
  

  
  #(1a)---- exact conditional minP test ---------
  pv.minP <- po.minPpval(X)
  if(pv.minP <= alpha){ dec.minP = 1 } else dec.minP=0
  #------------------------------------------------------
  
  #(1b)---- permutation minP test ---------
   pv.perm.minP <- perm.minPpval(X)
   if(pv.perm.minP <= alpha){ dec.perm.minP = 1 } else dec.perm.minP=0
  # dec.perm.minP=0
  #------------------------------------------------------
 
  
  #(2a)--- exact conditional likelihood ratio test -------
  pv.lr = po.LRpval(X)
  if(pv.lr<=alpha){dec.lr=1} else dec.lr=0 
  #------------------------------------------------------
  
  #(2b)--- permutation likelihood ratio test -------
   pv.perm.lr = perm.LRpval(X)
   if(pv.perm.lr<=alpha){dec.perm.lr=1} else dec.perm.lr=0 
  # dec.perm.lr=0 
  # ------------------------------------------------------
  

  #(3a)----- exact conditional CU.5 test -------
  pv.cu5 = po.cu5pval(X) 
  if(pv.cu5<=alpha){dec.cu5=1} else dec.cu5=0
  #------------------------------------------------------
  
  #(3b)----- permutation CU.5 test -------
   pv.perm.cu5 = perm.cu5pval(X) 
   if(pv.perm.cu5<=alpha){dec.perm.cu5=1} else dec.perm.cu5=0
  # dec.perm.cu5=0
  # ------------------------------------------------------
  
  
  #(4a)----- exact conditional CU1 test -------
  pv.cu1 = po.cu1pval(X) 
  if(pv.cu1<=alpha){dec.cu1=1} else dec.cu1=0
  #------------------------------------------------------
  
  #(4b)----- permutation CU1 test -------
   pv.perm.cu1 = perm.cu1pval(X) 
   if(pv.perm.cu1<=alpha){dec.perm.cu1=1} else dec.perm.cu1=0
  #dec.perm.cu1=0
  # ------------------------------------------------------
   
  

  
  return(cbind(dec.minP, dec.perm.minP, dec.lr, dec.perm.lr,
               dec.cu5, dec.perm.cu5, dec.cu1, dec.perm.cu1))
},  mc.set.seed = TRUE, mc.cores = no_cores)

#======= storing outputs and results ========
result = data.frame(do.call(rbind,output))

pow.minP = mean(result[,1]); pow.perm.minP = mean(result[,2])
pow.lr = mean(result[,3]); pow.perm.lr = mean(result[,4])
pow.cu5 = mean(result[,5]); pow.perm.cu5 = mean(result[,6])
pow.cu1 = mean(result[,7]); pow.perm.cu1 = mean(result[,8])

date()
tictoc::toc()


cat("pow.minP=", pow.minP,"\n");
cat("pow.perm.minP=", pow.perm.minP,"\n");
cat("pow.lr=", pow.lr,"\n");
cat("pow.perm.lr=", pow.perm.lr,"\n");
cat("pow.cu5=", pow.cu5,"\n");
cat("pow.perm.cu5=", pow.perm.cu5,"\n");
cat("pow.cu1=", pow.cu1,"\n");
cat("pow.perm.cu1=", pow.perm.cu1,"\n");


cat( pow.minP,"\n");
cat( pow.perm.minP,"\n");
cat( pow.lr,"\n");
cat( pow.perm.lr,"\n");
cat(pow.cu5,"\n");
cat(pow.perm.cu5,"\n");
cat(pow.cu1,"\n");
cat(pow.perm.cu1,"\n");


cat("T",T,"tau",tau,"lam1",lam1,"lam2",lam2,"theta",theta,"mcrun",mcrun,"\n")



#---- idea for presenting simulation ----------
# Try lam1=1,lam2=(1--6),T=10, tau=3
# Try lam1=2,lam2=(2--7),T=10, tau=5
# Try lam1=1,lam2=(1--5),T=10, tau=5
# Try lam1=0.1,lam2=(0.1--2),T=20, tau=5
# Try lam1=0.1,lam2=(0.1--1.2),T=50, tau=10
# Try lam1=2,lam2=(2--5),T=50, tau=10
